<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="com.onlinetutotingsystem.entity.CourseStatus"%>



<title>Student Home</title>
<style>
table {
	width: 60%;
}

th {
	text-align: left;
}

td {
	border-radius: 25px;
	background: grey;
	background-position: left top;
	background-repeat: repeat;
	padding: 0px, 10px, 10px, 5px;
	width: 20px;
	height: 15px;
	text-align: left;
}

#logout {
	align: right;
}
</style>
</head>

<body>
	<p style="font-size: 160%;">
		Welcome
		<%=request.getParameter("username")%>
	<p>
	<p>Session value</p>
	<%
		String username_value = (String) session.getAttribute("session_username");
		String password_value = (String) session.getAttribute("session_password");
		//out.println("logged in user: " + username_value);

		//out.println("\nlogged in user password: " + password_value);
		//out.println("session"+session);
	%>
	<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" style="float: right;">
	</form>
	<h3>Courses Available</h3>
	<p>Select the course you want to learn to know more about tutor,
		pricing and availability</p>
	<table class="table" frame="box" cellpadding="10" class="coursetable">
		<thead>
			<tr>
				<th scope="col" position="static">Course Name</th>
				<th scope="col">Available Tutors</th>
				<th scope="col">Check for availability</th>
			</tr>
		</thead>
		<tbody>
			<%
				Map<String, String> courselist = (HashMap<String, String>)request.getAttribute("resultset");

				for (Entry<String, String> entry : courselist.entrySet()) {

					out.print(entry.getKey());
					out.print(entry.getValue());
					out.print(
							"<tr>" + "<th scope=\"row\">" + entry.getKey() + "</th>" + "<td>" + entry.getValue() + "</td>" +
									 "<td><a href=/OnlineTutoringSystem/coursedetail?course=" + entry.getKey() + ">Other Pricing</a></td>"+
									"</tr>");

				}
			%>

		</tbody>
	</table>
	<br>
	<h3>Student completed/subscribed learnings</h3>
	<table class="table table-striped" frame="box" cellpadding="10">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Course Name</th>
				<th scope="col">Tutor Name</th>
				<th scope="col">Status</th>
			</tr>
		</thead>
		<tbody>
			<%
				ArrayList<CourseStatus> coursestatus = (ArrayList<CourseStatus>) request.getAttribute("coursestatus");
				int i = 1;
				
				for (CourseStatus coursestate : coursestatus) {
					out.print("<tr>" + "<th scope=\"row\">" + i + "</th>" + "<td>" + coursestate.getCourseName() + "</td>"
							+ "<td>" + coursestate.getTutorName() + "</td>" + "<td>" + coursestate.getStauts() + "</td>"
							 + "</tr>");
				i++;
				}
			%>
		</tbody>
	</table>


</body>
</html>