<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@page import="java.util.ArrayList"%>
<%@page import="com.onlinetutotingsystem.entity.CoursesOffering"%>
<title>Offerings over a course</title>
</head>
<body>
<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" style="float: right;">
	</form>
	<h2>Select the offering(s) and submit</h2>
	<table id="tableId" onclick="addRowHandlers()">
		<tr>
			<th>Checkbox</th>
			<th>Tutor Name</th>
			<th>pricing</th>
			<th>availability</th>
			<th>rating</th>
		</tr>
<%
	ArrayList<CoursesOffering> coursesoffering= (ArrayList<CoursesOffering>) request.getAttribute("coursesoffering");
	for(CoursesOffering courseoffer:coursesoffering){
		out.print("<tr id=\"Task\">"+
				"<td><input type=\"checkbox\"  name=\"selectcourses\" value="+courseoffer.getCourseid()+"></td>"+
				"<td>"+courseoffer.getTutorName()+"</td>"+
				"<td>"+courseoffer.getPricing()+"</td>"+
				"<td>"+courseoffer.getAvailability()+"</td>"+
				"<td>"+courseoffer.getRating()+"</td>"+
			"</tr>");
	}
%>
	</table>
	<form action="RegisterServlet" method="post">
	<input type="submit" value="Submit Your Request">
	</form>
	<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}
</style>

<script>
function getData(){
    var id = document.getElementById('Task').value;
    alert(id);
    out.prinltn("here it is"+id);
}


function addRowHandlers() {
	  var table = document.getElementById("tableId");
	  var rows = table.getElementsByTagName("tr");
	  for (i = 0; i < rows.length; i++) {
	    var currentRow = table.rows[i];
	    var createClickHandler = function(row) {
	      return function() {
	        var cell = row.getElementsByTagName("td")[0];
	        var id = cell.innerHTML;
	        alert("id:" + id);
	      };
	    };
	    currentRow.onclick = createClickHandler(currentRow);
	  }
	}
	
	
	
</script>
</body>
</html>