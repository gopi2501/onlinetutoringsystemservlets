package com.onlinetutoringsystem.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.onlinetutoringsystem.dao.CoursesDao;
import com.onlinetutotingsystem.entity.CoursesOffering;

/**
 * Servlet implementation class CourseOfferings
 */

public class CourseOfferings extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void oninit(ServletConfig config) {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(false);
		String role = (String) session.getAttribute("role");
		System.out.println("role is " + role);
		
		String course = (String) request.getParameter("course");
		System.out.println("course is " + course);
		if (role.equals("student")) {
			CoursesDao coursesdao2	= new CoursesDao();
			ArrayList<CoursesOffering> specificourses=coursesdao2.getSpecificCourses(course);
			response.setContentType("text/html");
			RequestDispatcher rd = request.getRequestDispatcher("subjectoffering.jsp");
			request.setAttribute("coursesoffering", specificourses);
			rd.forward(request, response);

		} else if (role.equals("tutor")) {
			CoursesDao coursesdao2	= new CoursesDao();
			ArrayList<CoursesOffering> specificourses=coursesdao2.getSpecificCourses(course);
		
			response.setContentType("text/html");
			RequestDispatcher rd = request.getRequestDispatcher("tutorsubjectoffering.jsp");
			request.setAttribute("coursesoffering", specificourses);
			rd.forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
