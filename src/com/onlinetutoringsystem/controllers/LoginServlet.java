package com.onlinetutoringsystem.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.onlinetutoringsystem.dao.CoursesDao;
import com.onlinetutoringsystem.dao.StudentDaoImplementation;
import com.onlinetutoringsystem.dao.TutorDaoImplementation;
import com.onlinetutoringsystem.services.StudentService;
import com.onlinetutoringsystem.services.TutorService;
import com.onlinetutotingsystem.entity.CourseStatus;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// response.setContentType("text/html");
		// PrintWriter out= response.getWriter();
		// String username=request.getParameter("username");
		// String password=request.getParameter("password");
		// // now go pass these values to the user services and find the users genuinity
		// //for now redirect to login successful page
		// RequestDispatcher reqdispatcher=
		// request.getRequestDispatcher("loginsuccessful.jsp");
		// reqdispatcher.forward(request, response);
		//// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGet(request, response);
		
		String role = request.getParameter("role");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		HttpSession session = request.getSession (true);
		session.setAttribute("session_username",username );
		session.setAttribute("session_password",password);
		

		// CASE 1: Case of Student
		if (role.equals("student")) {
			StudentService studentservice = new StudentService();
			if (studentservice.findStudent(username, password)) {
				session.setAttribute("role", "student");
				
				StudentDaoImplementation studentdao= new StudentDaoImplementation();
				int studentid=studentdao.findStudentId(username, password);
				session.setAttribute("student", studentdao.findStudent(username, password));
				CoursesDao coursedao = new CoursesDao();
				Map<String, String> mymap = coursedao.getAllCourses();
				
				ArrayList<CourseStatus> coursestatus = coursedao.studentCompletedorUpcomingteachings(studentid);
					
				
//				System.out.println("Result set obtained is " + mymap.toString());
				
				RequestDispatcher requestdispatcher = request.getRequestDispatcher("StudentHome.jsp");
				
//				System.out.println("Array list of items" + coursestatus);
				
				request.setAttribute("resultset", mymap);
				request.setAttribute("coursestatus", coursestatus);
				requestdispatcher.forward(request, response);
			
			} else {
				RequestDispatcher requestdispatcher = request.getRequestDispatcher("loginfailed.jsp");
				requestdispatcher.forward(request, response);
			}
		}

		// CASE 2: Case of tutor
		else if (role.equals("tutor")) {
			TutorService tutorservice = new TutorService();
			if (tutorservice.findTutor(username, password)) {
				session.setAttribute("role", "tutor");
				TutorDaoImplementation tutordao= new TutorDaoImplementation();
				int tutorid=tutordao.findTutorId(username, password);
				
				CoursesDao coursedao = new CoursesDao();
				Map<String, String> mymap = coursedao.getAllCourses();
			
				
				ArrayList<CourseStatus> coursestatus = coursedao.tutorCompletedorUpcomingteachings(tutorid);
				
				
				RequestDispatcher requestdispatcher = request.getRequestDispatcher("TutorHome.jsp");
				request.setAttribute("resultset", mymap);
				request.setAttribute("coursestatus", coursestatus);
				requestdispatcher.forward(request, response);
				
			} else {
				RequestDispatcher requestdispatcher = request.getRequestDispatcher("loginfailed.jsp");
				requestdispatcher.forward(request, response);
			}
		}

	}

}
