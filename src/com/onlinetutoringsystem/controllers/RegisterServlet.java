package com.onlinetutoringsystem.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.onlinetutoringsystem.services.StudentService;
import com.onlinetutoringsystem.services.TutorService;
import com.onlinetutotingsystem.entity.Student;
import com.onlinetutotingsystem.entity.Tutor;

public class RegisterServlet extends HttpServlet {
	public void oninit(ServletConfig config) {
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		String email=request.getParameter("email");
		String role=request.getParameter("role");
		String gender=request.getParameter("gender");
		java.sql.Date sqlStartDate = null;
		boolean pushstatus = false;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date;
		try {
			date = sdf1.parse(request.getParameter("dob"));
			sqlStartDate = new java.sql.Date(date.getTime()); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
//		response.setContentType("text/html");
//		PrintWriter out= response.getWriter();
//		out.print(sqlStartDate);
//		Date dob=(Date)request.getParameter("dob");
		
		if(role.equals("student")) {
			System.out.println("I am triggering student service");
			Student student= new Student(username, password, email, gender, sqlStartDate);
			StudentService studentservice=new StudentService();
			pushstatus=studentservice.insertStudent(student);
		} else if(role.equals("tutor")) {
			System.out.println("I am triggering tutor service");
//			String username,  String password, String email, String gender, Date dob
			Tutor tutor= new Tutor(username, password, email, gender, sqlStartDate);
			TutorService tutorservice= new TutorService();
			pushstatus= tutorservice.insertStudent(tutor);
			
		}
		
		if(pushstatus) {
			RequestDispatcher rd=request.getRequestDispatcher("loginsuccessful.jsp");
			rd.forward(request, response);
		}
		
		
	
	}
}
