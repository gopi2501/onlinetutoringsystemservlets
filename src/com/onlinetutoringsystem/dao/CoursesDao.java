package com.onlinetutoringsystem.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.onlinetutoringsystem.connections.ConnectionManager;
import com.onlinetutotingsystem.entity.Course;
import com.onlinetutotingsystem.entity.CourseStatus;
import com.onlinetutotingsystem.entity.CoursesOffering;
import com.onlinetutotingsystem.entity.Student;
import com.onlinetutotingsystem.entity.Tutor;

public class CoursesDao {
	
	public void registerRequestFromUser()
	{
		
	}
	

	public Map<String, String> getAllCourses() {
		System.out.println("CourseDAO: start of finding all the courses");
		Map<String, String> mymap = null;
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();
			String query = "SELECT COUNT(coursename) as countofcourses, coursename as coursename FROM course GROUP BY coursename";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Result set after execution is " + rs);
			Course course = new Course();
			// boolean more = rs.next();

			// if (!more) {
			// if (rs == null) {
			// System.out.println("oops i cannot find the user ");
			// // userexists=false;
			// // course.setName(rs.getString("coursename"));
			//
			// } else {
			// Map<String, String> mymap = new HashMap<>();
			mymap = new HashMap<>();
			while (rs.next()) {
				System.out.println(rs.getString("coursename") + " " + rs.getString("countofcourses"));
				mymap.put(rs.getString("coursename"), rs.getString("countofcourses"));
			}
			// }// close of the if loop

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		return mymap;
	}

	public ArrayList<CourseStatus> studentCompletedorUpcomingteachings(int studentid) {
		System.out.println("CourseDAO: start of finding student completed or upcomming teachings ");
		// Map<CourseStatus, CourseStatus> studentcourses = null;
		ArrayList<CourseStatus> coursestatusarray = null;
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();

			String query = "SELECT * FROM course where studentid='" + studentid + "'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Result set after execution is " + rs);
			CourseStatus coursestatus ;
			coursestatusarray = new ArrayList();

			// boolean more = rs.next();
			//
			// if (!more) {
			// if (rs == null) {
			// System.out.println("No Courses found ");
			//
			// } else {

			while (rs.next()) {
				coursestatus = new CourseStatus();
				coursestatus.setCourseName(rs.getString("coursename"));// 1. I have set course name
				// now write call to tutor dao requesting tutor details

				String tutorid = rs.getString("tutorid");
				System.out.println("Obtained tutor id is " + tutorid);
				TutorDaoImplementation tutordao = new TutorDaoImplementation();
				Tutor tutor = tutordao.findTutorById(Integer.parseInt(tutorid));
				coursestatus.setTutorName(tutor.getName());// 2. I have set tutor name

				StudentDaoImplementation studentdao = new StudentDaoImplementation();
				Student student = studentdao.findStudentById(Integer.parseInt(rs.getString("studentid")));
				coursestatus.setStudentName(student.getName());// 3. I have set student name

				Date courseEndDate = rs.getDate("enddate");
				Calendar currenttime = Calendar.getInstance();
				Date sqldate = new Date((currenttime.getTime()).getTime());
				int datedifference = sqldate.compareTo(courseEndDate);
				if (datedifference >= 0) {
					// completed course
					coursestatus.setStauts("@Completed");
				} else {
					// yet to complete
					Format formatter = new SimpleDateFormat("yyyy-MM-dd");
					String formatedDate = formatter.format(courseEndDate);
					coursestatus.setStauts("@targeted" + formatedDate);
				}

				System.out.println(coursestatus);
				coursestatusarray.add(coursestatus);
			}
			if (rs == null) {
				System.out.println("No courses found");
			}
			// }

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		return coursestatusarray;

	}

	public ArrayList<CourseStatus> tutorCompletedorUpcomingteachings(int tutorid) {
		System.out.println("CourseDAO: start of finding tutor completed or upcomming teachings ");
		// Map<CourseStatus, CourseStatus> studentcourses = null;
		ArrayList<CourseStatus> facultycoursestatusarray = null;
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();

			String query = "SELECT * FROM course where tutorid='" + tutorid + "'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Result set after execution is " + rs);
			CourseStatus coursestatus;
			facultycoursestatusarray = new ArrayList();
			while (rs.next()) {
				coursestatus = new CourseStatus();
				coursestatus.setCourseName(rs.getString("coursename"));// 1. I have set course name
				// now write call to tutor dao requesting tutor details

				String tutoridd = rs.getString("tutorid");
				System.out.println("Obtained tutor id is " + tutoridd);
				TutorDaoImplementation tutordao = new TutorDaoImplementation();
				Tutor tutor = tutordao.findTutorById(Integer.parseInt(tutoridd));
				coursestatus.setTutorName(tutor.getName());// 2. I have set tutor name
				System.out.println("The tutor i found is :"+ tutor.getName());
				
				StudentDaoImplementation studentdao = new StudentDaoImplementation();
				Student student = studentdao.findStudentById(Integer.parseInt(rs.getString("studentid")));
				coursestatus.setStudentName(student.getName());// 3. I have set student name

				Date courseEndDate = rs.getDate("enddate");
				Calendar currenttime = Calendar.getInstance();
				Date sqldate = new Date((currenttime.getTime()).getTime());
				int datedifference = sqldate.compareTo(courseEndDate);
				if (datedifference >= 0) {
					// completed course
					coursestatus.setStauts("@Completed");
				} else {
					// yet to complete
					Format formatter = new SimpleDateFormat("yyyy-MM-dd");
					String formatedDate = formatter.format(courseEndDate);
					coursestatus.setStauts("@targeted" + formatedDate);
				}

				System.out.println("The course i found under a tutor is "+coursestatus);
				facultycoursestatusarray.add(coursestatus);
			}
			if (rs == null) {
				System.out.println("No courses found");
			}
			// }

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		System.out.println("The courses list for tutor call "+facultycoursestatusarray.toString());
		return facultycoursestatusarray;

	}
	
	public ArrayList<CoursesOffering> getSpecificCourses(String coursename) {
		
		System.out.println("CourseDAO: start of finding specfic for student courses");
		ArrayList<CoursesOffering> mymap = null;
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();
			String query = "SELECT * FROM course where coursename='"+coursename+"'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Result set after execution is " + rs);
			CoursesOffering courseoffering;
			
			mymap = new ArrayList();
			while (rs.next()) {
				courseoffering = new CoursesOffering();
				courseoffering.setAvailability(rs.getString("slots"));
				courseoffering.setPricing(rs.getInt("price"));
				courseoffering.setCourseName(coursename);
				courseoffering.setCourseid(rs.getInt("courseid"));
				//get tutor 
				TutorDaoImplementation tutordao= new TutorDaoImplementation();
				Tutor tutor=tutordao.findTutorById(Integer.parseInt(rs.getString("tutorid")));

				courseoffering.setTutorName(tutor.getName());
				courseoffering.setPricing(tutor.getRating());

				mymap.add(courseoffering);
			}
			if(rs==null) {
				System.out.println("I didnt find any related courses");
			}
			// }// close of the if loop

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		return mymap;
	}
	
}
