package com.onlinetutoringsystem.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.onlinetutoringsystem.connections.ConnectionManager;
import com.onlinetutotingsystem.entity.Student;
import com.onlinetutotingsystem.entity.Tutor;

public class StudentDaoImplementation {
	Connection conn;

	public boolean insertStudent(Student student) {
		System.out.println("StudentDAO: start of inserting student");
		String username = student.getUsername();
		String password = student.getPassword();
		String email = student.getEmail();
		String gender = student.getGender();
		Date dob = student.getDob();

		String userupdate = "";
		boolean userinsert = false;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;

		try {
			con = ConnectionManager.getConnection();
			String query = "insert into student(username,password,email,gender,dob) values (?,?,?,?,?)";
			preparedstatement = con.prepareStatement(query);
			preparedstatement.setString(1, username);

			preparedstatement.setString(2, password);
			preparedstatement.setString(3, email);
			preparedstatement.setString(4, gender);
			preparedstatement.setDate(5, dob);

			int i = preparedstatement.executeUpdate();
			if (i != 0) {
				System.out.println("User added successfully");
				// userupdate= "success";
				userinsert = true;
			} else {
				System.out.println("Registration got failed");
				// userupdate="failed";
				userinsert = false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return userinsert;
	}

	public boolean findStudent(String username, String password) {
		System.out.println("StudentDAO: start of student dao upon login");
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();
			String query = "select * from student where username='" + username + "' AND password='" + password + "'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Statement is " + rs);

			// boolean more = rs.next();

			// if (!more) {
			// System.out.println("oops i cannot find the user ");
			// userexists=false;
			// } else {
			while (rs.next()) {
				System.out.println("printing out result set");
				String usernamefromdb = rs.getString("username");
				String email = rs.getString("email");
				String gender = rs.getString("gender");
				Date dob = rs.getDate("dob");
				userexists = true;

			} if(rs==null) {
				System.out.println("Cound not find student");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		Student stu = new Student();
		return userexists;
	}

	public boolean updateStudent(Student student) {

		return true;

	}

	public boolean deleteStudent(Student student) {

		return true;
	}

	public Student findStudentById(int studentid) {
		System.out.println("StudentDAO: start of find student by id ");
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement ps = null;
		Student student = new Student();
		con = ConnectionManager.getConnection();
		String query = "select * from student where studentid=" + studentid + ";";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				student.setName(rs.getString("username"));
				student.setUsername(rs.getString("username"));
				student.setGender(rs.getString("gender"));
				student.setEmail(rs.getString("email"));
			}
			if(rs==null) {
				System.out.println("no such student");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return student;
	}
	
	public int findStudentId(String username, String password) {
		System.out.println("StudentDAO: finding student id ");
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		int studentid = 0;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists = false;
		try {
			con = ConnectionManager.getConnection();
			String query = "select * from student where username='" + username + "' AND password='" + password + "'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Statement is " + rs);

			while (rs.next()) {
				System.out.println("printing out result set");
				studentid= rs.getInt("studentid");

			} if(rs==null) {
				System.out.println("Cound not find student");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		return studentid;
	}

}
