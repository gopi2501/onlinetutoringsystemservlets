package com.onlinetutoringsystem.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;
import com.onlinetutoringsystem.connections.ConnectionManager;
import com.onlinetutotingsystem.entity.Student;
import com.onlinetutotingsystem.entity.Tutor;

public class TutorDaoImplementation {
	
	Connection conn;
	
	public boolean insertTutor(Tutor tutor) {
		
		System.out.println("TutorDAO: start of inserting tutor");
		String username = tutor.getUsername();
		String name=tutor.getUsername();
		String password = tutor.getPassword();
		String email = tutor.getEmail();
		String gender=tutor.getGender();
		Date dob=tutor.getDob();
		
		
		String userupdate="";
		boolean userinsert=false;
		java.sql.Connection con=null;
		java.sql.PreparedStatement preparedstatement=null;
		
		try {
			con=ConnectionManager.getConnection();
			String query="insert into tutor(username,password,email,gender,dob) values (?,?,?,?,?)";
			preparedstatement= con.prepareStatement(query);
			preparedstatement.setString(1, username);
			
			preparedstatement.setString(2, password);
			preparedstatement.setString(3, email);
			preparedstatement.setString(4, gender);
			preparedstatement.setDate(5, dob);
			
			int i=preparedstatement.executeUpdate();
			if(i!=0) {
				System.out.println("Tutor added successfully");
//				userupdate= "success";
				userinsert=true;
			} else {
				System.out.println("Tutor Registration got failed");
//				userupdate="failed";
				userinsert=false;
			}	
		
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return userinsert;
	}
	
	public boolean findTutor(String username,String password) {
		System.out.println("TutorDAO:Start of finding tutor upon login");
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		java.sql.PreparedStatement preparedstatement = null;
		boolean userexists=false;
		try {
			con = ConnectionManager.getConnection();
			String query = "select * from tutor where username='" + username + "' AND password='" + password + "'";
			System.out.println("Statement before execution is "+query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Statement is "+rs);
			
			boolean more = rs.next();

			if (!more) {
				System.out.println("oops i cannot find the user ");
				userexists=false;
			} else {

				System.out.println("printing out result set");
				String usernamefromdb= rs.getString("username");
				String email=rs.getString("email");
				String gender=rs.getString("gender");
				Date dob= rs.getDate("dob");
				userexists=true;
				
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		Student stu = new Student();
		return userexists;
	
	}
	public Tutor findTutorById(int tutorid) {
		System.out.println("TutorDAO: Start of find tutor by id");
		ResultSet rs= null;
		Statement stmt=null;
		java.sql.Connection con=null;
		java.sql.PreparedStatement ps= null;
		Tutor tutor= new Tutor();
		con= ConnectionManager.getConnection();
		String query="select * from tutor where tutorid="+tutorid+";";
		try {
			stmt=con.createStatement();
			rs=stmt.executeQuery(query);
//			if(rs!=null) {
			while(rs.next()) {
				tutor.setName(rs.getString("username"));
				System.out.println("Username of the tutor"+rs.getString("username"));
				tutor.setUsername(rs.getString("username"));
				tutor.setGender(rs.getString("gender"));
				tutor.setEmail(rs.getString("email"));	
				tutor.setRating(rs.getInt("rating"));
			} if(rs==null) {
				System.out.println("No such tutor found");
			}
				
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tutor;
	}
	public boolean updateTutor(Student student) {
		
		return true;
		
	}
	
	public boolean deleteTutor(Student student) {
		
		return true;
	}
	
	
	public int findTutorId(String username, String password) {
		System.out.println("TutorDAO: finding student id ");
		ResultSet rs = null;
		Statement stmt = null;
		java.sql.Connection con = null;
		int tutorid = 0;
		java.sql.PreparedStatement preparedstatement = null;
	
		try {
			con = ConnectionManager.getConnection();
			String query = "select * from tutor where username='" + username + "' AND password='" + password + "'";
			System.out.println("Statement before execution is " + query);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("Statement is " + rs);

			while (rs.next()) {
				System.out.println("printing out result set");
				tutorid= rs.getInt("tutorid");

			} if(rs==null) {
				System.out.println("Cound not find tutor");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
				rs = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}

				con = null;
			}
		}
		return tutorid;
	}
	
	
	

}
