package com.onlinetutoringsystem.services;

import com.onlinetutoringsystem.dao.StudentDaoImplementation;
import com.onlinetutoringsystem.dao.TutorDaoImplementation;
import com.onlinetutotingsystem.entity.Tutor;

public class TutorService {

	public boolean insertStudent(Tutor tutor) {
		TutorDaoImplementation daoimplementation = new TutorDaoImplementation();
		// boolean result= daoimplementation.insertStudent(student);
		// Student student=new Student();

		return daoimplementation.insertTutor(tutor);
	}

	public boolean findTutor(String username, String password) {

		TutorDaoImplementation daoimplementation = new TutorDaoImplementation();
		boolean isuservalid = daoimplementation.findTutor(username, password);

		return isuservalid;

	}

}
