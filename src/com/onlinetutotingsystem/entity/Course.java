package com.onlinetutotingsystem.entity;

import java.sql.Date;

public class Course {

	private int price;
	private String name, payment, slots;
	private Date startDate, endDate;
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	public String getSlots() {
		return slots;
	}
	public void setSlots(String slots) {
		this.slots = slots;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Course(int price, String payment, Date startDate, Date endDate, String slots, String name) {
		super();
		this.name=name;
		this.slots=slots;
		this.price = price;
		this.payment = payment;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	public Course() {
		super();
	}
	
	
	
}
