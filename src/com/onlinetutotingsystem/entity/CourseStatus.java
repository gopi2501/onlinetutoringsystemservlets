package com.onlinetutotingsystem.entity;

public class CourseStatus {
	
	private String courseName, tutorName, stauts, studentName;


	public CourseStatus() {
		super();
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTutorName() {
		return tutorName;
	}

	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}

	public String getStauts() {
		return stauts;
	}

	public void setStauts(String stauts) {
		this.stauts = stauts;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	@Override
	public String toString() {
		return "CourseStatus [courseName=" + courseName + ", tutorName=" + tutorName + ", stauts=" + stauts
				+ ", studentName=" + studentName + "]";
	}
	
	

}
