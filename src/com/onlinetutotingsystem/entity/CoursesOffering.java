package com.onlinetutotingsystem.entity;

public class CoursesOffering {
	private int courseid;
	private String courseName;
	private String tutorName;
	private int pricing, rating;
	private String availability;
		
	
	public int getCourseid() {
		return courseid;
	}
	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getTutorName() {
		return tutorName;
	}
	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}
	public int getPricing() {
		return pricing;
	}
	public void setPricing(int pricing) {
		this.pricing = pricing;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public CoursesOffering() {
		super();
	}
	public CoursesOffering(String courseName,String tutorName, int pricing, int rating, String availability) {
		super();
		this.courseName=courseName;
		this.tutorName = tutorName;
		this.pricing = pricing;
		this.rating = rating;
		this.availability = availability;
	}
	
	
}
