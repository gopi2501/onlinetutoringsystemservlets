package com.onlinetutotingsystem.entity;

public class User {

	private String username, password, email, role, gender, dob;
	
	public User() {
		
	}

	public User(String username, String password, String email, String role, String gender, String dob) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.role = role;
		this.gender = gender;
		this.dob = dob;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	

}
